@extends('layout.master')

@section('judul')
Halaman List Berita
@endsection



@section('content')
<form>
@csrf
  <div class="form-group">
    <label for="exampleFormControlInput1">Judul Berita</label>
    <input type="text" class="form-control" id="exampleFormControlInput1">
  </div>
  <div class="form-group">
    <label for="exampleFormControlTextarea1">Content</label>
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
  </div>
  <div class="form-group">
    <label for="exampleFormControlSelect1">Kategori</label>
    <select class="form-control" id="exampleFormControlSelect1">
      <option>Wisata</option>
      <option>Kuliner</option>
    </select>
  </div>
  <form>
  <div class="form-group">
    <label for="exampleFormControlFile1">Thumbnail</label>
    <input type="file" class="form-control-file" id="exampleFormControlFile1">
  </div>
  <input type="submit" value="Submit">
  
</form>
</form>
@endsection