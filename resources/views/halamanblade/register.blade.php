@extends('layout.master')

@section('judul')
Halaman Form
@endsection
    
@section('content')
    <h1> Buat Account Baru </h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label>First Name :</label><br>
        <input type="text" name="first"><br> <br>
        <label> Last Name : </label> <br>
        <input type="text" name="last"><br><br>
        <label> Gender </label><br><br>
        <input type="radio" name="male"> Male<br>
        <input type="radio" name="female"> Female <br><br>
        <label> Nationality </label><br><br>
        <select name="nasionality" id="">
            <option value="1">Indonesia</option>
            <option value="2">Inggris</option>
            <option value="3">Jerman</option>
            <option value="4">Jepang</option><br><br>
        <select name="language spoken"></select><br><br>
        <label>Language Spoken</label><br><br>
        <input type="checkbox" name="Language Spoken"> Bahasa Indonesia <br>
        <input type="checkbox" name="Language Spoken"> English <br>
        <input type="checkbox" name="Language Spoken"> Other <br><br>
        <label>Bio</label><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea><br>
        </select>
        <input type="submit" value="sign up">
</form>
@endsection