<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "Indexcontroller@index");

Route::get('/form', "Authcontroller@bio");

Route::post('/welcome', "Authcontroller@kirim");

Route::get('/data-table', function(){
    return view('table.data-table');
});


Route::group(['middleware'=>['auth']],function(){
 //CRUD Cast
    //Create
    Route::get('/cast/create', "CastController@create"); //route menuju form create
    Route::post('/cast', "CastController@store"); //route untuk menyimpan data ke database
    //read
    Route::get('/cast', "CastController@index"); //route list cast
    Route::get('/cast/{cast_id}','CastController@show'); //route detail cast
    //update
    Route::get('/cast/{cast_id}/edit', 'CastController@edit'); //route menuju ke form edit
    Route::put('/cast/{cast_id}', 'CastController@update');//route untuk update data berdasarkan id di database
    //delete
    Route::delete('/cast/{cast_id}', 'CastController@destroy'); // route untuk hapus data di database 
    
    //update profile
    Route::resource('profile','ProfileController')->only([
        'index','update'
    ]);
    
});

 


Route::resource('berita','BeritaController');
Auth::routes();
Route::get('/berita', 'BeritaController@berita');
Route::get('/views/cast/createberita', 'CreateBerita@createberita');


