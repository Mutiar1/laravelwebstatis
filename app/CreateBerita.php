<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreateBerita extends Model
{
    protected $table ='berita';
    protected $fillable = ['judul','content','kategori','thumbnail'];
}