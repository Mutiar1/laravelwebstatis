<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
    protected $table ='berita';
    protected $filable = ['judul','content','kategori','thumbnail'];
}
